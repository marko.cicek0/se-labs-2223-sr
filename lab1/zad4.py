def lowercase_count(string):
    count = 0;
    allowed_chars = "abcdefghijklmnopqrstuvwyz"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

def uppercase_count(string):
    count = 0;
    allowed_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

def numbers_count(string):
    count = 0;
    allowed_chars = "'0','1','2','3','4','5','6','7','8','9'"

    for char in string:
        if char in allowed_chars:
            count += 1

    return count

def char_types_count(string):

    u = uppercase_count(string)
    l = lowercase_count(string)
    n = numbers_count(string)

    print(f"({u},{l},{n})")

def main():

    string = []
    string = str(input("Unesi string: "))

    char_types_count(string)

if __name__ == "__main__":
    main()
