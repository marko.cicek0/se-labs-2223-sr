#Input the comma-separated integers and transform them into a list of integers.
#Print the number of even ints in the given list. Note: the % "mod" operator computes the remainder,
#  e.g. 5 % 2 is 1.
#Use a function for counting evens.

#        count_evens([2, 1, 2, 3, 4]) → 3
#        count_evens([2, 2, 0]) → 3
#        count_evens([1, 3, 5]) → 0

def count_evens(num):
    count = 0;

    for int in num:
        if int % 2 == 0:
            count += 1

    print(f"Polje ima {count} parnih brojeva.")

def main():
    string = []
    string = str(input("Unesi brojeve: "))

    strings = string.split(",")

    numbers = []
    for i in strings:
        numbers.append(int(i))

    count_evens(numbers)

if __name__ == "__main__":
    main()
