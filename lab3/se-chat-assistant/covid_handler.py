from query_handler_base import QueryHandlerBase
import random
import requests
import json

class CovidHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "covid" in query:
            return True
        return False

    def process(self, query):
            result = self.call_api()
            self.ui.say("Hvala i ne zaboravite masku!")
 

    def call_api(self):
        url = "https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/"

        headers = {
            "X-RapidAPI-Key": "a8d6beae81msh7f09248643df24fp156346jsn9fa72b942431",
            "X-RapidAPI-Host": "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)

        parsed_data = json.loads(response.text)
        print(parsed_data)
        