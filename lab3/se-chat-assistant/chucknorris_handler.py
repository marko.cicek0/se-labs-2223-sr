from query_handler_base import QueryHandlerBase
import random
import requests
import json

class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "chuck" in query:
            return True
        return False

    def process(self, query):
        try:
            result = self.call_api()
            text = result["value"]
            self.ui.say(f"{text}")
        except: 
            self.ui.say("I can handle this request but I won't!")

    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        headers = {
	        "accept": "application/json",
	        "X-RapidAPI-Key": "a8d6beae81msh7f09248643df24fp156346jsn9fa72b942431",
	        "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)
        return json.loads(response.text)
